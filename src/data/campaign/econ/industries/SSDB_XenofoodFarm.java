package data.campaign.econ.industries;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.impl.campaign.econ.impl.BaseIndustry;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.campaign.econ.CommoditySpecAPI;
import com.fs.starfarer.api.campaign.econ.Industry;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Pair;
import com.fs.starfarer.api.util.Misc;
import java.awt.Color;
import org.apache.log4j.Logger;

public class SSDB_XenofoodFarm extends BaseIndustry {
    /*
    Xenofood Farms are a far substitute that generate food based on size regardless of planet.
    Generates food based on population, consumes Heavy Machinery equal to a hydroponics farm of similar size
    If alpha core was installed, generate a small amount of lobsters
    Has a stability malus of 1
    Can stack with Farming or Agriculture
    */
    private static final int STABILITY_MALUS = -1;
    private static final String STRUCTURE_ID = "ssdb_xenofarm";
    private static final String BASE_VALUE_TEXT = "Base value for colony size";
    static Logger log = Global.getLogger(SSDB_XenofoodFarm.class);
    private boolean aiCore = false;
    
    @Override
    public void apply(){
        super.apply(true);
        int marketSize = market.getSize();
        
        modifyStabilityWithBaseMod(); //Apply stabilty penalty
        
        demand(0, Commodities.HEAVY_MACHINERY, marketSize/2, BASE_VALUE_TEXT); //Demands Heavy Machinery equal to hydroponics

        Pair<String, Integer> deficit = getMaxDeficit(Commodities.HEAVY_MACHINERY); //Get deficits
        log.info("Deficit: " + deficit.one + "," + deficit.two);
        log.error("DEBUG CORE BIZ: "+aiCore+","+market.getIndustry(STRUCTURE_ID).getAICoreId());
        //Ripped from SCY, credit Tartiflette
        if(aiCore && (market.getIndustry(STRUCTURE_ID).getAICoreId()==null)|| !market.getIndustry(STRUCTURE_ID).getAICoreId().equals(Commodities.ALPHA_CORE)) {
            log.info("Do not Lobsters for player.");
            supply(Commodities.LOBSTER,0);
            aiCore=false;
        }
        else {
            log.info("Generate"+ marketSize/3 +" Lobsters for player");
            supply(Commodities.LOBSTER, marketSize/3 );
            aiCore=true;
        }
	supply(Commodities.FOOD, marketSize - deficit.two); //Generate food stocks
	if (!isFunctional()) {	supply.clear();	} //If structure not functional, do not generate anything
    }
    
    @Override
    public void unapply() { 
        log.info("Unapplying effects");
        super.unapply();    //Unapply building
    }
    
    @Override
    public int getBaseStabilityMod() {
        return STABILITY_MALUS;     //Set variable for stability mod
    }

    @Override
    public String getUnavailableReason() {
	return super.getUnavailableReason(); //Return why industry unavailable
    }
    
    @Override
    public boolean isAvailableToBuild() {
    	return super.isAvailableToBuild(); //Can build
    }

    @Override
    public boolean showWhenUnavailable() {
        return super.showWhenUnavailable(); //Show if unavailable
    }
    
    @Override
    public void createTooltip(Industry.IndustryTooltipMode mode, TooltipMakerAPI tooltip, boolean expanded) {
        super.createTooltip(mode, tooltip, expanded); //Create tooltip
    }
    
    //Alpha core bonus: Generate Lobsters
    @Override
    protected void addAlphaCoreDescription(TooltipMakerAPI tooltip, Industry.AICoreDescriptionMode mode) {
        float opad = 10f;
        Color highlight = Misc.getHighlightColor();
        String pre = "Alpha-level AI core currently assigned. ";
        if (mode == Industry.AICoreDescriptionMode.MANAGE_CORE_DIALOG_LIST || mode == Industry.AICoreDescriptionMode.INDUSTRY_TOOLTIP) {
            pre = "Alpha-level AI core. ";
        }
        if (mode == Industry.AICoreDescriptionMode.INDUSTRY_TOOLTIP) {
            CommoditySpecAPI coreSpec = Global.getSettings().getCommoditySpec(aiCoreId);
            TooltipMakerAPI text = tooltip.beginImageWithText(coreSpec.getIconName(), 48);
            text.addPara(pre + "Enables this industry to produce %s.", 0f, highlight,"Volturnian Lobsters");
            tooltip.addImageWithText(opad);
            return;
        }
        tooltip.addPara(pre + "Enables this industry to produce %s.", 0f, highlight,"Volturnian Lobsters");
    }
    
}
