DarkerBusiness is a game modification for the upcoming indie space game 'Starsector' (http://fractalsoftworks.com/).

The goal of this mod is to add additional colony structures that grant new, powerful options at a price. 
For each of the structures, the primary design ethos should be to give the player a Faustian bargain - you may have X, but at a cost of Y.
Theme of these structures should have criminal/shady undertones to reflect the self-destructive nature of these structures.

Ex.
```
Wildcat Mining Operations
This structure generates a small amount of each of the core basic resource commodities (ore, rare ore, organics, volatiles) and may be built regardless of planetary modifiers.
This structure has an consumes Recreational Drugs, Heavy Machinery and reduces colony stabilty by 2.
```